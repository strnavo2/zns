//-------------------------------------------------------------------------
//
//  BI-ZNS: Šablona pro úlohu č.4 - CART
//  (c) 2021 Ladislava Smítková Janků <ladislava.smitkova@fit.cvut.cz>
//
//-------------------------------------------------------------------------

#include "cart.h"
#include <bits/stdc++.h>
#include <iterator>
#include <limits.h>
#include <float.h>
#include <iostream>
#include <math.h>

using namespace std;

vector<string> features;
vector<vector<string>> values;
vector<vector<string>> rows;

bool verbose = true;

bool meetsTheConditions1( vector<string> & row, map<string, string> & conditions) {
    bool meets = false;
    map<string, string>::iterator it;

    for (it = conditions.begin(); it != conditions.end(); it++) {
        for ( string & r : row )
            if ( it->second == r )
                meets = true;
        if ( meets == false )
            return false;
        else
            meets = false;
    }

    return true;
}

bool meetsTheConditions2( string & feature, map<string, string> & conditions ) {

    if ( conditions.find(feature) != conditions.end() )
        return false;
    return true;
}

string decision( vector<string> & r ) {
    string yesOrNo = "no";

    if ( r.back() == "1" )
        yesOrNo = "yes";
    
    return yesOrNo;
}

double calculateGiniIndex( map<string, string> conditions, int selectedFeatureIndex) {
    // check for errors

    if (conditions.find( features[ selectedFeatureIndex]) != conditions.end())
        throw string ("Sestavuji index pro feature, jejíž hodnotu mám ale jako podmínku.");

    // calculate index
    double gini = 0;
    map<string, string> conditionsVal = conditions;

    for ( string & v : values[selectedFeatureIndex]) {

//      cout << "v = " << v << endl;

        conditionsVal[ features[ selectedFeatureIndex]] = v;
        double rowsCond = 0;
        double yes = 0;
        double no = 0;

        for( vector<string> & r : rows ) {
            if (meetsTheConditions1( r, conditions)) {
                rowsCond = rowsCond + 1;
                if (meetsTheConditions1( r, conditionsVal)) {                   
                    if (decision(r) == "yes")
                        yes++;
                    else
                        no++;
                }
            }
        }

        if ((yes > 0) || (no > 0)) {

//          cout << "calculating gini" << endl;
//          cout << "yes, no, rowsCond = " << yes << ", " << no << ", " << rowsCond << endl;

            double index = 1.0 - pow( yes / (yes + no), 2) - pow( no / (yes + no), 2);

//          cout << "yes / (yes + no) = " << yes / (yes + no) << endl;
//          cout << "pow( yes / (yes + no), 2) = " << pow( yes / (yes + no), 2) << endl;
//          cout << "pow( no / (yes + no), 2) = " << pow( no / (yes + no), 2) << endl;
//          cout << "pow( yes / (yes + no), 2) + pow( no / (yes + no), 2) = " << pow( yes / (yes + no), 2) + pow( no / (yes + no), 2) << endl;
//          cout << "index = " << index << endl;

            gini += index * (yes + no) / rowsCond;

//          cout << "gini = " << gini << endl;
        }
    }

//  cout << "gini returned = " << gini << endl;

    return gini;
}

TNode * createTree( map<string, string> conditions) {

    cout << endl;
    cout << "createTree" << endl;

    // test jestli podmínka neimplikuje pořád stejné rozhodnutí
    int yes = 0;
    int no = 0;
    for( vector<string> & r : rows ) {
        if (meetsTheConditions1( r, conditions)) {
            if (decision(r) == "yes")
                yes++;

//              cout << "yes++" << endl;

            else
                no++;

//              cout << "no++" << endl;

        }
    }

//  cout << "Number of YES: " << yes << endl;
//  cout << "Number of NO: " << no << endl;

    cout << "   conditions: ";
    map<string, string>::iterator it;

    for (it = conditions.begin(); it != conditions.end(); it++) {
        cout << it->first << " = " << it->second << ",";
    }
    cout << endl;

    if (yes == 0) {
        if (no == 0)
           throw string ("Nelze tvořit strom: yes==0 && no==0");
        cout << "   ==> no" << endl;
        return new TLeaf(0);

//      cout << "I choose NO!" << endl;
//      TNode * tList = new TLeaf("no");
//      cout << "   ==> " << tList->toString().c_str() << endl;
//      cout << "new TLeaf no: " << tList->toString().c_str();
//      return tList;

    } else {
        if (no == 0) {
            cout << "   ==> yes" << endl;
            return new TLeaf(1);

//          cout << "I choose YES!" << endl;
//          TNode * tList = new TLeaf("yes");
//          cout << "   ==> " << tList->toString().c_str() << endl;
//          cout << "new TLeaf yes: " << tList->toString().c_str();
//          return tList;
        }
    }

    // create parent node
    double minimum = DBL_MAX;
    double winner = -1;
    for ( int f = 0; f <= (values.size())-1; f++ ) {
        if (meetsTheConditions2( features[f], conditions)) {

//          cout << "Gini(" << features[ f] << ") ?" << endl;

            double gini = calculateGiniIndex( conditions, f);

            cout << "   Gini(" << features[ f] << ") = " << gini << endl;
//          cout << "gini = " << gini << endl;

            if (gini < minimum) {
                minimum = gini;
                winner = f;
            }
        }
    }


//  cout << "Gini(" << features[ winner] << ") = " << minimum << endl;


    if (winner < 0)
        throw string ("no winner");


//  cout << "Creating left, right." << endl;


    conditions[ features[ winner]] = values[ winner][0];
    TNode * left = createTree( conditions);
    conditions[ features[ winner]] = values[ winner][1];
    TNode * right = createTree( conditions);


    cout << "winner: " << features[ winner] << endl;


    // podle feature winner se bude rozhodovat pro vetev left nebo right
    return new TParent( winner, left, right);
}



TNode *cart()
{
    if (verbose) printf("Vytvářím rozhodovací strom...\n");

    map<string, string> conditions;

    return createTree(conditions);
}
